package hospital;

public class Medico {
    private String especialidad;
    private Empleado empleado;

    public Medico(){
    }
    public Medico(String especialidad, int numero_empleado, String nombre_completo_empleado){
        this.especialidad = especialidad;
        this.empleado = new Empleado(numero_empleado, nombre_completo_empleado);
    }
    public String getEspecialidad() {
        return this.especialidad;
    }
    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }
    public Empleado getEmpleado() {
        return this.empleado;
    }
    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("---Medico---");
        sb.append("\nEspecialidad del Medico: ").append(especialidad);
        sb.append(empleado);
        sb.append("\n***Medico***");
        return sb.toString();
    }
}
