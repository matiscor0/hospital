package hospital;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;

public class Analisis {
    private int numero_referencia;
    private String tipo_analisis;
    private int anio;
    private int mes;
    private int dia;
    private Medico medico_solicita;
    private String resultado_analisis;


    public Analisis() {
    }

    public Analisis(int numero_referencia, String tipo_analisis, String fecha_realizado,
                    String especialidad, int numero_empleado, String nombre_completo_empleado 
                    ,String resultado_analisis){
                        this.numero_referencia = numero_referencia;
                        this.tipo_analisis = tipo_analisis;
                        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
                        try {
                        java.util.Date fecha = formato.parse(fecha_realizado);
                        LocalDate fechaLocal = LocalDate.ofInstant(fecha.toInstant(), ZoneId.systemDefault());
                        this.anio = fechaLocal.getYear();
                        this.mes = fechaLocal.getMonthValue();
                        this.dia = fechaLocal.getDayOfMonth();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        this.medico_solicita = new Medico(especialidad, numero_empleado, nombre_completo_empleado);
                        this.resultado_analisis = resultado_analisis;
                    }
    public int getNumero_referencia() {
        return numero_referencia;
    }
    public void setNumero_referencia(int numero_referencia) {
        this.numero_referencia = numero_referencia;
    }
    public String getTipo_analisis() {
        return tipo_analisis;
    }
    public void setTipo_analisis(String tipo_analisis) {
        this.tipo_analisis = tipo_analisis;
    }
    public void setFecha_realizado(){}
    public Medico getMedico_solicita() {
        return medico_solicita;
    }
    public void setMedico_solicita(Medico medico_solicita) {
        this.medico_solicita = medico_solicita;
    }
    public String getResultado() {
        return resultado_analisis;
    }
    public void setResultado(String resultado_analisis) {
        this.resultado_analisis = resultado_analisis;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("---Analisis---");
        sb.append("\nNumero de referencia: ").append(numero_referencia);
        sb.append("\nTipo de analisis: ").append(tipo_analisis);
        sb.append("\nFecha realizado: ").append(dia);
        sb.append("/").append(mes);
        sb.append("/").append(anio);
        sb.append("\nSolicitado por: \n").append(medico_solicita);
        sb.append("\nResultado de analisis: ").append(resultado_analisis);
        sb.append("\n***Analisis***");
        return sb.toString();
    }
}
