package hospital;

public class Empleado {
    private int numero_empleado;
    private String nombre_completo_empleado;



    public Empleado() {
    }

    public Empleado(int numero_empleado, String nombre_completo_empleado){
        this.numero_empleado = numero_empleado;
        this.nombre_completo_empleado = nombre_completo_empleado;
    }

    public int getNumero_empleado() {
        return this.numero_empleado;
    }
    public void setNumero_empleado(int numero_empleado) {
        this.numero_empleado = numero_empleado;
    }
    public String getNombre_completo_empleado() {
        return this.nombre_completo_empleado;
    }
    public void setNombre_completo_empleado(String nombre_completo_empleado) {
        this.nombre_completo_empleado = nombre_completo_empleado;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nNumero de empleado: ").append(numero_empleado);
        sb.append("\nNombre de empleado: ").append(nombre_completo_empleado);
        return sb.toString();
    }
}
