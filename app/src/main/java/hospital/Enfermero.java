package hospital;

public class Enfermero {
    private Empleado empleado_enf;


    public Enfermero(){
    }
    public Enfermero(int numero_empleado, String nombre_completo_empleado){
        this.empleado_enf = new Empleado(numero_empleado, nombre_completo_empleado);
    }
    public Empleado getEmpleado_enf() {
        return this.empleado_enf;
    }
    public void setEmpleado_enf(Empleado empleado_enf) {
        this.empleado_enf = empleado_enf;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("---Enfermero---");
        sb.append(empleado_enf);
        sb.append("\n***Enfermero***");
        return sb.toString();
    }
}
