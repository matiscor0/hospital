package hospital;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;

public class Persona {
    private String nombre_completo;
    private int dni;
    private int anio;
    private int mes;
    private int dia;
    private Direccion direccion;
    private String telefono;
    private String email;

    public Persona(){
    }
    public Persona(String nombre_completo,int dni, String fecha_nacimiento, String telefono,
                    String email, String provincia, String localidad, String barrio, String calle, 
                    int numero_calle){
        this.nombre_completo = nombre_completo;
        this.dni = dni;
        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
        try {
            java.util.Date fecha = formato.parse(fecha_nacimiento);
            LocalDate fechaLocal = LocalDate.ofInstant(fecha.toInstant(), ZoneId.systemDefault());
            this.anio = fechaLocal.getYear();
            this.mes = fechaLocal.getMonthValue();
            this.dia = fechaLocal.getDayOfMonth();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.telefono = telefono;
        this.email = email;
        this.direccion = new Direccion(provincia, localidad, barrio, calle, numero_calle);
    }
    public String getNombre_completo() {
        return this.nombre_completo;
    }
    public void setNombre_completo(String nombre_completo) {
        this.nombre_completo = nombre_completo;
    }
    public int getDni() {
        return this.dni;
    }
    public void setDni(int dni) {
        this.dni = dni;
    }
    public void setFecha_nacimiento(Date fecha_nacimiento) {
    }
    public String getTelefono() {
        return this.telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public Direccion getDireccion() {
        return this.direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("---Informacion personal---\nNombre completo: ").append(nombre_completo);
        sb.append("\nDNI: ").append(dni);
        sb.append("\nFecha nacimiento: ").append(dia);
        sb.append("/").append(mes);
        sb.append("/").append(anio);
        sb.append("\nTelefono: ").append(telefono);
        sb.append("\nEmail: ").append(email);
        sb.append("\n").append(direccion);
        sb.append("\n***Informacion personal***\n");
        return sb.toString();
    }
}
