package hospital;

public class HistoriaClinica {
    private Analisis analisis_realizado;
    private String diagnostico;
    private String tratamiento;


    public HistoriaClinica(){
    }

    public HistoriaClinica(int numero_referencia, String tipo_analisis, String fecha_realizado,
    String especialidad, int numero_empleado, String nombre_completo_empleado 
    ,String resultado_analisis, String diagnostico, String tratamiento){

        this.analisis_realizado = new Analisis(numero_referencia, tipo_analisis, fecha_realizado, especialidad, numero_empleado, nombre_completo_empleado, resultado_analisis);
        this.diagnostico = diagnostico;
        this.tratamiento = tratamiento;
    }
    public Analisis getAnalisis_realizado() {
        return this.analisis_realizado;
    }
    public void setAnalisis_realizado(Analisis analisis_realizado) {
        this.analisis_realizado = analisis_realizado;
    }
    public String getDiagnostico() {
        return this.diagnostico;
    }
    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }
    public String getTratamiento() {
        return this.tratamiento;
    }
    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("---Historia Clinica---\n");
        sb.append(analisis_realizado);
        sb.append("\nDiagnostico: ").append(diagnostico);
        sb.append("\nTratamiento: ").append(tratamiento);
        sb.append("\n***Historia Clinica***\n");
        return sb.toString();
    }

}
