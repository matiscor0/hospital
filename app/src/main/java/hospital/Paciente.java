package hospital;

public class Paciente extends Persona{
    private HistoriaClinica historia;
    private Enfermero enfermero;

    public Paciente(){}

    public Paciente(String nombre_completo,int dni, String fecha_nacimiento, String telefono,
    String email, String provincia, String localidad, String barrio, String calle, 
    int numero_calle, int numero_referencia, String tipo_analisis, String fecha_realizado,
    String especialidad, int numero_empleado, String nombre_completo_empleado 
    ,String resultado_analisis, String diagnostico, String tratamiento){
        super(nombre_completo, dni, fecha_nacimiento, telefono, email, provincia, localidad, barrio, calle, numero_calle);
        this.historia = new HistoriaClinica(numero_referencia, tipo_analisis, fecha_realizado, especialidad, numero_empleado, nombre_completo_empleado, resultado_analisis, diagnostico, tratamiento);
        this.enfermero = new Enfermero(numero_empleado, nombre_completo_empleado);
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("---Paciente---\n");
        sb.append(super.toString());
        sb.append(historia);
        sb.append(enfermero);
        sb.append("\n***Paciente***");
        return sb.toString();
    }
}
