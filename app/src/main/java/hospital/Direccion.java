package hospital;

public class Direccion {
    public String provincia;
    public String localidad;
    public String barrio;
    public String calle;
    public int numero_calle;



    public Direccion(){
        
    }
    public Direccion(String provincia, String localidad, String barrio, String calle ,int numero_calle ) {
        this.provincia = provincia;
        this.localidad = localidad;
        this.barrio = barrio;
        this.calle = calle;
        this.numero_calle = numero_calle;
    }
    public String getProvincia() {
        return this.provincia;
    }
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }
    public String getLocalidad() {
        return this.localidad;
    }
    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }
    public String getBarrio() {
        return this.barrio;
    }
    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }
    public String getCalle() {
        return this.calle;
    }
    public void setCalle(String calle) {
        this.calle = calle;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Provincia: ").append(provincia);
        sb.append("\nLocalidad: ").append(localidad);
        sb.append("\nBarrio: ").append(barrio);
        sb.append("\nCalle: ").append(calle);
        sb.append("\nNumero: ").append(numero_calle);
        return sb.toString();
    }

}
