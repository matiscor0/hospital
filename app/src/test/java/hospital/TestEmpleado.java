package hospital;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
public class TestEmpleado {
    @Test
    public void verificarEmpleado(){
        Empleado empleado1 = new Empleado(2308,"Lucas Romero");

        assertEquals(empleado1.toString(),"\nNumero de empleado: 2308\nNombre de empleado: Lucas Romero");
    
    }
}
