package hospital;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
public class TestPersona {
    @Test
    public void verificarPersona(){
        Persona persona1 = new Persona("matias cordoba",43270667,
        "2001/04/25","3834", "matiscor0@gmail",
        "Catamarca","San Fernando del Valle de Catamarca", "Tablada",
        "Guemes",22);


        assertEquals(persona1.toString(), "---Informacion personal---\n" +
        "Nombre completo: matias cordoba\n" +
        "DNI: 43270667\n" +
        "Fecha nacimiento: 25/4/2001\n" +
        "Telefono: 3834\n" +
        "Email: matiscor0@gmail\n" +
        "Provincia: Catamarca\n" +
        "Localidad: San Fernando del Valle de Catamarca\n" +
        "Barrio: Tablada\n" +
        "Calle: Guemes\n" +
        "Numero: 22" +
        "\n***Informacion personal***\n");
    }
}
