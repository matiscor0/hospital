package hospital;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
public class TestAnalisis {
    @Test
    public void verificarAnalisis(){
        Analisis analisis1 = new Analisis(230,"Análisis de sangre",
                                            "2023/04/27","Cardiologo",
                                            2308,"Lucas Romero",
                                            "Anemia");

        assertEquals(analisis1.toString(), "---Analisis---"+ 
        "\nNumero de referencia: 230" + 
        "\nTipo de analisis: Análisis de sangre"+ 
        "\nFecha realizado: 27/4/2023"+
        "\nSolicitado por: " +
        "\n---Medico---" +
        "\nEspecialidad del Medico: Cardiologo"+
        "\nNumero de empleado: 2308"+
        "\nNombre de empleado: Lucas Romero" +
        "\n***Medico***" +
        "\nResultado de analisis: Anemia" +
        "\n***Analisis***");

    }
}
