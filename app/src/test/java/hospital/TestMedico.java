package hospital;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
public class TestMedico {
    @Test
    public void verificarMedico(){
        Medico medico = new Medico("Cardiologo",2308,"Lucas Romero");

        assertEquals(medico.toString(),"---Medico---"+
        "\nEspecialidad del Medico: Cardiologo"+
        "\nNumero de empleado: 2308"+
        "\nNombre de empleado: Lucas Romero" +
        "\n***Medico***");
    }
}