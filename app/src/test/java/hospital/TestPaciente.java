package hospital;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestPaciente {
    @Test
    public void verificarPaciente() {
        Paciente paciente1 = new Paciente("Juan Perez", 12345678, "1990/01/01", "555-1234",
            "juan@example.com", "Buenos Aires", "La Plata", "Centro", "Calle Falsa", 123,
            456, "Sangre", "2022/05/15", "Cardiología", 789, "Lucas Romero",
            "Negativo", "Arritmia", "Medicación");

        assertEquals(paciente1.toString(), "---Paciente---" +
            "\n---Informacion personal---\n" +
            "Nombre completo: Juan Perez\n" +
            "DNI: 12345678\n" +
            "Fecha nacimiento: 1/1/1990\n" +
            "Telefono: 555-1234\n" +
            "Email: juan@example.com\n" +
            "Provincia: Buenos Aires\n" +
            "Localidad: La Plata\n" +
            "Barrio: Centro\n" +
            "Calle: Calle Falsa\n" +
            "Numero: 123" +
            "\n***Informacion personal***" +
            "\n---Historia Clinica---" +
            "\n---Analisis---" +
            "\nNumero de referencia: 456\n" +
            "Tipo de analisis: Sangre\n" +
            "Fecha realizado: 15/5/2022\n" +
            "Solicitado por: "+
            "\n---Medico---"+
            "\nEspecialidad del Medico: Cardiología\n" +
            "Numero de empleado: 789\n" +
            "Nombre de empleado: Lucas Romero" +
            "\n***Medico***"+
            "\nResultado de analisis: Negativo" +
            "\n***Analisis***"+
            "\nDiagnostico: Arritmia\n" +
            "Tratamiento: Medicación" +
            "\n***Historia Clinica***" +
            "\n---Enfermero---\n" +
            "Numero de empleado: 789\n" +
            "Nombre de empleado: Lucas Romero"+
            "\n***Enfermero***" +
            "\n***Paciente***");
    }
}
