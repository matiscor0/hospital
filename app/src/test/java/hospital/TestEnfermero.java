package hospital;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
public class TestEnfermero {
    @Test
    public void verificarEnfermero(){
        Enfermero enfermero = new Enfermero(208,"Lucas Romero");

        assertEquals(enfermero.toString(), "---Enfermero---"+
        "\nNumero de empleado: 208"+
        "\nNombre de empleado: Lucas Romero"+
        "\n***Enfermero***");
    }
}
