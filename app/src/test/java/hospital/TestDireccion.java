package hospital;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestDireccion {
    @Test
    public void verificarDireccion(){
        Direccion direccion = new Direccion("Catamarca","San Fernando del Valle de Catamarca", "Tablada",
        "Guemes",22);

        assertEquals(direccion.toString(),"Provincia: Catamarca"+
        "\nLocalidad: San Fernando del Valle de Catamarca"+
        "\nBarrio: Tablada"+
        "\nCalle: Guemes"+
        "\nNumero: 22");

    }
}
    