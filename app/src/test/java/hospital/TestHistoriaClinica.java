package hospital;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestHistoriaClinica {
    @Test
    public void verificarHistoriaClinica(){
        HistoriaClinica historia1 = new HistoriaClinica(230,"Análisis de sangre",
        "2023/04/27","Cardiologo",
        2308,"Lucas Romero",
        "Anemia","Falta de vitaminas","Ingerir alimentos");


        assertEquals(historia1.toString(),"---Historia Clinica---\n"+
        "---Analisis---"+ 
        "\nNumero de referencia: 230" + 
        "\nTipo de analisis: Análisis de sangre"+ 
        "\nFecha realizado: 27/4/2023"+
        "\nSolicitado por: " +
        "\n---Medico---" +
        "\nEspecialidad del Medico: Cardiologo"+
        "\nNumero de empleado: 2308"+
        "\nNombre de empleado: Lucas Romero" +
        "\n***Medico***" +
        "\nResultado de analisis: Anemia" +
        "\n***Analisis***" +
        "\nDiagnostico: Falta de vitaminas" +
        "\nTratamiento: Ingerir alimentos" +
        "\n***Historia Clinica***\n");
    }
}
